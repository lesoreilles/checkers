# This is a sample Python script.

# Press Maj+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.

# https://python.doctor/page-tkinter-interface-graphique-python-tutoriel

from tkinter import *
from tkinter.messagebox import *
from fenetre import *
import fenetre


def dessiner_echiquier(fen):
    canvas = fen.getCanvas()
    compteur = 0

    for y in range(10):
        for x in range(10):
            x1 = x*40
            y1 = y*40

            print (str(x1)+"/"+str(y1))

            if compteur%2==1:
                # Fonctionne
                #canvas.create_rectangle(x1,y1,x1+40,y1+40,fill="red")

                # Ne fonctionne pas
                photo = PhotoImage(file="img_damier_jeu_vide.gif")
                canvas.create_image(x1,y1,anchor=NW,image=photo)
            else:
                # Fonctionne
                #canvas.create_rectangle(x1,y1,x1+40,y1+40,fill="yellow")

                # Ne fonctionne pas
                photo = PhotoImage(file="img_damier_non_jeu.gif")
                canvas.create_image(x1,y1,anchor=NW,image=photo)

            compteur = compteur+1

        compteur = compteur+1



def action_nouvelle_partie():
    showinfo("Alerte","Nouvelle partie")


def action_quitter():
    sys.exit()
    #showinfo("Alerte","Quitter")


def callback_click_canvas(event):
    showinfo("Alerte",str(event.x)+" "+str(event.y))


fenetre = Tk()
fenetre.geometry("400x500")
fenetre.title("checkers")
fenetre.configure(background='white')

menubar = Menu(fenetre)
menu_action = Menu(menubar,tearoff=0)
menu_action.add_command(label="Nouvelle partie",command=action_nouvelle_partie)
menu_action.add_separator()
menu_action.add_command(label="Quitter", command=action_quitter)
menubar.add_cascade(label="Action",menu=menu_action)
fenetre.configure(menu=menubar)

canvas = Canvas(fenetre,width=400, height=400, background="white")
fen = Fenetre(canvas)
dessiner_echiquier(fen)
canvas.bind("<Button-1>",func=callback_click_canvas)
canvas.pack(side = TOP)

label = Label(fenetre,text="message pour l'utilisateur",bg='white')
label.pack(side = BOTTOM)

fenetre.mainloop()

